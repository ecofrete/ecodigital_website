#!/usr/bin/env bash
# export keys of AWS
export AWS_ACCESS_KEY_ID=AKIAJAPWWGQ3R3RCHJRA
export AWS_SECRET_ACCESS_KEY=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
export AWS_DEFAULT_REGION=us-east-1

# make sure all docs were removed from the S3 bucket before we deployed the new ones, so we wouldn’t run into any left overs.
aws s3 rm s3://www.ecodigital.co --recursive

# Deploying to the S3 Bucket
# As static pages typically don’t change very often we wanted to have caching in place, so the page is loaded quickly. By setting the cache-control header we make sure all parts of our documentation are cached for one day by the browser. The –acl public-read flag makes sure all files in the bucket are accessible for everyone.

aws s3 sync dist s3://www.ecodigital.co --acl public-read --cache-control "public, max-age=86400"

