angular.module('webApp')
  .service('ecoAuth', ['config','$http', '$log', '$q', function(config, $http, $log, $q) {
    var _token = window.sessionStorage.token;
    var storeToken = function(){
      window.sessionStorage.token = _token;
    };
    var getToken = function(){
      return _token;
    };

    /**
     * Authentication based on app_key and domain origin SSl
     * @param data
     * @returns {promise.promise|jQuery.promise|qFactory.Deferred.promise|jQuery.ready.promise|ud.h.promise}
     */
    var authenticate = function(data){
      var that = this;
      var deferred = $q.defer();
      $http.post(config.apiUrl + config.paths.auth,{},
        {
          headers: {
            'X-rest-api-key': data.APP_KEY
            //, 'x-rest-api-key': data.APP_KEY
          }
        }
      )
        .success(function(data) {
          //Store token ID
          if(!data.error)
          setToken(data.token);
          // console.log(data);
          $http.defaults.headers.common['Authorization'] = 'OS.1';
          //resolve defer
          deferred.resolve({
            result: (!data.error) ? data : data
          });
        }).error(function(msg, code) {
          deferred.reject(msg);
          delete window.sessionStorage.token;
          $log.error(msg, code);
        });
      return deferred.promise;
    };

    this.auth = function(PK){
      var data = {
        APP_KEY: PK
      };
      return authenticate(data);
    };

    var setToken = function(token){
      _token = token;
      storeToken(_token);
    };

    this.getToken = function(){
      return getToken();
    };

  }]);
