'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('MainCtrl', function ($scope, $translate, $window, $http, ecoAuth) {

    $scope.fname = '';
    $scope.email = '';
    $scope.msg = '';
    $scope.website = '';
    $scope.emailnews='';
    var endPoint='http://api.ecodigital.co/bo/register/email';
    var endPointNews='http://api.ecodigital.co/bo/register/newsletter';
    
    // var endPoint     ='http://localhost:8000/bo/register/email';
    // var endPointNews ='http://localhost:8000/bo/register/newsletter';
    
    // ecoAuth.auth('eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJFY29kaWdpdGFsIEtleSIsImlhdCI6MTQ2NTUxNTc1MCwiZXhwIjoxNDk3MDUxNzUwLCJpc3MiOiJlY29kaWdpdGFsLmNvIiwiYXVkIjoiZWNvZGlnaXRhbC5jbyJ9.ivbwbUZ6fCmShybPt99-lR7doL-V8KlayVAPPF7KpEc', function(r){
    ecoAuth.auth('OS.1', function(data){
      //authentication      
      //$http.defaults.headers.common['Authorization'] = data.oauth_token;
    });

    $scope.sendContact = function(){
      $scope.formOk = true;

      if($scope.fname ==''){
        $scope.formOk = false;
      }else
      if($scope.website ==''){
        $scope.formOk = false;
      }else
      if($scope.email ==''){
        $scope.formOk = false;
      }

      if($scope.formOk == true){
        try{

          $http.post(endPoint, { name: $scope.fname, email: $scope.email
            , msg: $scope.msg, website: $scope.website  }).success(function(res) {
            alert('Thank you. Obrigado. Gracias !');
          });
        }catch(err){
          alert('Ocorreu um problema no envio, por favor enviar email para contato@ecodigital.co');
        }
      }else{
        alert('Faltam informação, por favor preencher todos os campos. Missing informations, please fill all fields.');
      }
      $scope.showModal = false;
    };

    $scope.sendNewsletter = function(email){
      try{
        $http.post(endPointNews, { email: email }).success(function(res) {          
            alert('Thank you. Obrigado. Gracias !');
        });
      }catch(err){
        alert('Ocorreu um problema no envio, por favor enviar email para contato@ecodigital.co');
      }
    };


    //change header position when fixed
    angular.element($window).bind("scroll", function() {
      if (this.pageYOffset >= 100) {
        $scope.fixedHeader = true;
      } else {
        $scope.fixedHeader = false;
      }
      $scope.$apply();
    });

    $scope.changeLanguage = function (key) {
      $translate.use(key);
      ga('send', 'event', 'website', 'change_lang', 'change_lang');
    };

    $scope.showModal = false;
    $scope.trialBtn = function(active){
      $scope.showModal = active;
      if(active){
        ga('send', 'event', 'website', 'trial-true', 'open form');
      } else{
        ga('send', 'event', 'website', 'trial-false', 'close form');
      }
    };

    $scope.subscribeBtn = function(){
      if($scope.emailnews==''){
        alert('Please, input your email. Por favor informar seu email.');
      }else{
        ga('send', 'event', 'website', 'subscribe', 'Subscribe news');
        $scope.sendNewsletter($scope.emailnews);
      }
    };



















  });
