'use strict';

/**
 * @ngdoc overview
 * @name webApp
 * @description
 * # webApp
 *
 * Main module of the application.
 */
angular
  .module('webApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    //'ngTouch',
    'ui.router',
    'pascalprecht.translate',
    'smoothScroll'

  ]).config(function ( $stateProvider, $urlRouterProvider, $locationProvider, $translateProvider) {

    $translateProvider.translations('es', {
      BENEFICIOS: 'Beneficios',
      DOCUMENTACAO: 'Documentacion',
      SELO: 'Sello Sostenible',
      CERTIFICADO: 'Certificación Digital',
      CERTIFICADO_TITLE_TEXT: 'La certificación digital, simples y fáciles',
      CERTIFICADO_TEXT : 'Con Ecodigital usted certifica nombre de usuario y le da su poder para contribuir al medio ambiente',

      SELO_TEXT : 'SELLO SOSTENIBLE le da a su sitio web o aplicación plataforma integrada garantizo que neutraliza las emisiones en el MEDIO AMBIENTE.',


      CONTATO: 'Contacto',
      SOBRE_NOS : 'Sobre Nosotros',

      SAIBA_MAIS: 'VER MÁS',

      HERO_TITULO: 'CERTIFICADO DIGITAL DE <span class=" is-bold">SUSTENTABILIDADE</span> ',
      HERO_SUBTITULO: 'Certificado y Sello digital del EMPRESAS SE PREOCUPE CON NUESTRO PLANETA EL FUTURO. ADOPTAR esta idea y atraer a los consumidores fieles.',
      HERO_BUTTON_CALL: 'PROBAR POR 30 DIAS',

      FOOTER_BUTTON_1 : 'CALENTAMIENTO GLOBAL está matando nuestro planeta.',
      FOOTER_BUTTON_2 : 'AJUDE PROJETOS SOCIAIS QUE GERAM IMPACTO NA SOCIEDADE.',
      FOOTER_BUTTON_3 : 'Las empresas que participan ganar más credibilidad.',

      TEASER_MIDDLE_TITULO : 'Beneficios sean sostenibles',
      TEASER_MIDDLE_SUBTITULO : 'Parte de una comunidad de negocios que se preocupa por nuestro futuro. Conectar con sus clientes a través de una cola noble.',
      FOOTER_MIDDLE_BUTTON_1 : 'EMPRESAS PREOCUPAN AL AMBIENTE',
      FOOTER_MIDDLE_BUTTON_2 : 'Conectar SU NEGOCIO DIGITAL CON proyectos sostenibles.',
      FOOTER_MIDDLE_BUTTON_3 : 'SELLO SOSTENIBLE PARA TI Y PARA EL CERTIFICADO su consumidor.',


      CLIENTES_TITULO   : 'Clientes',
      CONTATO_TITULO    : 'Contacto',
      CONTATO_NOME      : 'Nombre',
      CONTATO_ASSUNTO   : 'Sujeto',
      CONTATO_MEIO   : 'Contacto Medio',
      CONTATO_TEL   : 'Teléfone',
      CONTATO_EMAIL   : 'Email',
      CONTATO_MSG : 'Mensaje' ,
      CONTATO_EMPRESA   : 'Empresa',
      CONTATO_ENVIAR   : 'Enviar',
      CONTATO_CANCELAR   : 'Cancelar',
      CONTATO_WEBSITE   : 'Sitio Web',


      ECO_APP_TEXT: 'Creado especialmente para las aplicaciones móviles.',
      ECO_TAXI_TEXT: 'Para aquellos que caminan en taxi, también puede ayudar a compensar.',
      ECO_DELIVERY_TEXT: 'Si pide alimentos, emite CO2 en la atmósfera, borraremos.',
      ECO_FRETE_TEXT: 'Hecho especialmente para el mercado de comercio electrónico.',
      ECO_SITE_TEXT: 'Ideal para aquellos que quieren neutralizar las emisiones realizadas por los servidores.',
      ECO_EARTH_TEXT: 'Sello tiene todas las otras modalidades.',

      NEWSLETTER : 'Recibe nuestras noticias de plataforma!',

      INSCREVER : 'Ingressar'

    });

    $translateProvider.translations('en-us', {
      BENEFICIOS: 'Benefits',
      DOCUMENTACAO: 'Documentation',
      SELO: 'Sustainable Seal',
      CERTIFICADO: 'Digital Certification',
      CERTIFICADO_TITLE_TEXT: 'Digital certification, simples & easy',
      CERTIFICADO_TEXT : 'With Ecodigital you certify your Username and gives her power to contribute to the environment',
      SELO_TEXT : 'SUSTAINABLE SEAL GIVES YOUR WEBSITE OR APP PLATFORM INTEGRATED GUARANTEE YOU neutralizes EMISSIONS IN THE ENVIRONMENT.',


      CONTATO: 'Contact',
      SOBRE_NOS : 'About us',

      SAIBA_MAIS: 'SEE MORE',

      HERO_TITULO: 'DIGITAL CERTIFICATE OF <span class=" is-bold">SUSTAINABILITY</span> COMPANIES',
      HERO_SUBTITULO: 'CERTIFICATE AND SEAL DIGITAL COMPANIES WORRY WITH OUR PLANET THE FUTURE. ADOPT THIS IDEA AND ATTRACT CONSUMERS faithful.',
      HERO_BUTTON_CALL: 'TRIAL FOR 30 DAYS',

      FOOTER_BUTTON_1 : 'GLOBAL WARMING IS KILLING OUR PLANET.',
      FOOTER_BUTTON_2 : 'SOCIAL AID PROJECTS THAT GENERATE IMPACT ON SOCIETY.',
      FOOTER_BUTTON_3 : 'COMPANIES PARTICIPATING EARN MORE CREDIBILITY.',

      TEASER_MIDDLE_TITULO : 'Benefits to be sustainable',
      TEASER_MIDDLE_SUBTITULO : 'Part of a business community that cares about our future. Connect with your customers through a noble tail.',
      FOOTER_MIDDLE_BUTTON_1 : 'COMPANIES WORRY TO THE ENVIRONMENT',
      FOOTER_MIDDLE_BUTTON_2 : 'Connect Digital BUSINESS WITH SUSTAINABLE PROJECTS.',
      FOOTER_MIDDLE_BUTTON_3 : 'SUSTAINABLE SEAL FOR YOU AND CERTIFICATION FOR ITS CONSUMERS.',


      CLIENTES_TITULO   : 'Clientes',
      CONTATO_TITULO    : 'Contact',
      CONTATO_NOME      : 'Name',
      CONTATO_ASSUNTO   : 'Subject',
      CONTATO_MEIO   : 'Contact channel',
      CONTATO_TEL   : 'Telephone',
      CONTATO_EMAIL   : 'Email',
      CONTATO_MSG : 'Message' ,
      CONTATO_EMPRESA   : 'Company',
      CONTATO_ENVIAR   : 'Send',
      CONTATO_CANCELAR   : 'Cancel',
      CONTATO_WEBSITE   : 'Website',



      ECO_APP_TEXT: 'Created especially for mobile APPs.',
      ECO_TAXI_TEXT: 'For those who walk by taxi, can you help also compensate.',
      ECO_DELIVERY_TEXT: 'If you order food, emits Co2 in the atmosphere, we will delete.',
      ECO_FRETE_TEXT: 'Made especially for the Ecommerce market.',
      ECO_SITE_TEXT: 'Ideal for those who want to neutralize the emissions made by servers.',
      ECO_EARTH_TEXT: 'Seal has all the other modalities.',

      NEWSLETTER : 'News every week!',

      INSCREVER : 'Regiter'
    });

    $translateProvider.translations('pt-br', {
      BENEFICIOS: 'Benefícios',
      DOCUMENTACAO: 'Documentação',
      SELO: 'Selo sustentável',
      CERTIFICADO: 'Certificado digital',
      CERTIFICADO_TITLE_TEXT: 'Certificado digital simples e fácil',
      CERTIFICADO_TEXT : 'Com a Ecodigital você certifica seu usuário e dá o poder dela contribuir com o meio ambiente',
      SELO_TEXT : 'Selo sustentável dá ao seu website, app ou plataforma integrada a garantia que você neutraliza emissões no meio ambiente.',

      CONTATO: 'Contato',
      SOBRE_NOS : 'Sobre Nós',

      SAIBA_MAIS: 'SAIBA MAIS',

      HERO_TITULO: 'CERTIFICADO DIGITAL DE <span class=" is-bold is-new-green-text" > SUSTENTABILIDADE </span> ',
      HERO_SUBTITULO: 'CERTIFICADO E SELO DIGITAL PARA EMPRESAS QUE SE PREOCUPAM COM O FUTURO DO NOSSO PLANETA. ADOTE ESSA IDEIA E ATRAIA CONSUMIDORES FIEIS.',
      HERO_BUTTON_CALL: 'EXPERIMENTE POR 30 DIAS',

      FOOTER_BUTTON_1 : 'REDUZA AS EMISSÕES DE CO2 NO MEIO AMBIENTE DE FORMA SIMPLES E FÁCIL.',
      FOOTER_BUTTON_2 : 'CONTRIBUA COM DIVERSOS PROJETOS SOCIOAMBIENTAIS DE IMPACTO NA SOCIEDADE.',
      FOOTER_BUTTON_3 : 'TORNE-SE UMA EMPRESA SUSTENTÁVEL E ENGAGE SEUS CLIENTES POSITIVAMENTE.',

      TEASER_MIDDLE_TITULO : 'Ser <span class=" is-bold is-new-green-text" >sustentável</span> é bom para os negócios',
      TEASER_MIDDLE_SUBTITULO : 'Faça parte de uma comunidade empresarial que se preocupa com nosso futuro. Conecte-se com seus consumidores através de uma cauda nobre.',
      FOOTER_MIDDLE_BUTTON_1 : 'EMPRESAS QUE SE PREOCUPAM COM O MEIO AMBIENTE',
      FOOTER_MIDDLE_BUTTON_2 : 'CONECTAMOS SEU NEGÓCIO DIGITAL COM PROJETOS SUSTENTÁVEIS.',
      FOOTER_MIDDLE_BUTTON_3 : 'SELO SUSTENTÁVEL PARA VOCÊ E CERTIFICADO PARA SEU CONSUMIDOR.',




      CLIENTES_TITULO   : 'Clientes',
      CONTATO_TITULO    : 'Contato',
      CONTATO_NOME      : 'Nome',
      CONTATO_ASSUNTO   : 'Assunto',
      CONTATO_MEIO   : 'Meio de contato',
      CONTATO_TEL   : 'Telefone',
      CONTATO_EMAIL   : 'Email',
      CONTATO_MSG : 'Mensagem' ,
      CONTATO_EMPRESA   : 'Empresa',
      CONTATO_ENVIAR   : 'Enviar',
      CONTATO_CANCELAR   : 'Cancelar',
      CONTATO_WEBSITE   : 'Website',



      ECO_APP_TEXT: 'Criado especialmente para APPs mobile.',
      ECO_TAXI_TEXT: 'Quem anda de táxi pode ajudar a compensar também.',
      ECO_DELIVERY_TEXT: 'Se pedir comida emite CO² na atmosfera, vamos eliminar',
      ECO_FRETE_TEXT: 'Feito especialmente para o mercado de Ecommerce.',
      ECO_SITE_TEXT: 'Ideal para quem quer neutralizar as emissões feitas por servidores.',
      ECO_EARTH_TEXT: 'Selo que possui todas as demais modalidades.',

      NEWSLETTER : 'Receba notícias da nossa plataforma!',

      INSCREVER : 'Inscrever-se'




    });
    $translateProvider.preferredLanguage('pt-br');
    $translateProvider.useLocalStorage();





    $urlRouterProvider
      .otherwise('/');
    $locationProvider.html5Mode(true);

    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .state('feed', {
        url: '/feed',
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })

    ;

    $locationProvider.html5Mode(true);
  })
  .run(function ($rootScope, $location, $state) {

    $rootScope.appTitle = 'Ecodigital | O futuro em um click';
    $rootScope.app = {
      api : '/api',
      locals:[],
      state : $state
    };

    //$rootScope.$on('$stateChangeStart', function (event, next) {
    //      $location.path('/boards');
    //});

  }).constant('config', {
    appVersion: 1.2,
    apiUrl: 'http://api.ecodigital.co',
    // apiUrl: 'http://localhost:8000',
    paths : {
      auth   : '/auth'
    }
  });
